<?php

return [
    'name' => 'Einstellungen',

    'fields' => [
        'shared' => [
            'active' => 'Aktiv',
            'true' => 'Ja',
            'false' => 'Nein'
        ],
        'app' => [
            'title' => 'Allgemein',
            'app_name_label' => 'App-Name',
            'app_name_description' => 'Der allgemeine Name dieser App',
            'app_brand_logo_label' => 'Logo',
            'app_brand_logo_description' => 'Das Logo in diesem Backend',
            'app_brand_signet_label' => 'Signet',
            'app_brand_signet_description' => 'Das Signet in diesem Backend'
        ],
        'analytics' => [
            'title' => 'Analytics',
            'system_label' => "Modus",
            'system_description' => 'Welche Art von Tracking soll aktiviert werden?'
        ],
        'uptimemonitor' => [
            'title' => 'UptimeMonitor'
        ],
        'admin' => [
            'title' => 'Admin'
        ]
    ]
];
