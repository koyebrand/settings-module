<?php

return [
    'name' => 'Settings',

    'fields' => [
        'shared' => [
            'active' => 'active',
            'true' => 'true',
            'false' => 'false'
        ],
        'app' => [
            'title' => 'General',
            'app_name_label' => 'App-name',
            'app_name_description' => 'The name of this app for general use',
            'app_brand_logo_label' => 'Logo',
            'app_brand_logo_description' => 'The logo in this backend',
            'app_brand_signet_label' => 'Signet',
            'app_brand_signet_description' => 'The signet in this backend'
        ],
        'analytics' => [
            'title' => 'Analytics',
            'system_label' => "Mode",
            'system_description' => 'Which type of tracking should be activated?'
        ],
        'uptimemonitor' => [
            'title' => 'UptimeMonitor'
        ],
        'admin' => [
            'title' => 'Admin'
        ]
    ]
];