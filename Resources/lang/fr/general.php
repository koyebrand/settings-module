<?php

return [
    'name' => 'Préférences',

    'fields' => [
        'shared' => [
            'active' => 'active',
            'true' => 'true',
            'false' => 'false'
        ],
        'app' => [
            'title' => 'Général',
            'app_name_label' => 'Nom de l\'application',
            'app_name_description' => 'Le nom de cette application pour un usage général',
            'app_brand_logo_label' => 'Logo',
            'app_brand_logo_description' => 'Le logo de ce backend',
            'app_brand_signet_label' => 'Signet',
            'app_brand_signet_description' => 'Le sigle dans ce backend'
        ],
        'analytics' => [
            'title' => 'Analytics',
            'system_label' => "Mode",
            'system_description' => 'Quel type de suivi doit être activé?'
        ],
        'uptimemonitor' => [
            'title' => 'UptimeMonitor'
        ],
        'admin' => [
            'title' => 'Admin'
        ]
    ]
] ;