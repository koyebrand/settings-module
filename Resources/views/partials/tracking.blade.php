@if(setting('analytics_system','none')=='own_matomo')
    @if(setting("analytics_id",'') != "" && setting("analytics_tracker_url",'') != "")
        
        <!-- Matomo -->
        @script
        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="{{ setting("analytics_tracker_url",'') }}/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '{{ setting("analytics_id",'') }}']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
        @endscript
        <!-- End Matomo Code -->

    @else
        <!-- NO Matomonot enough info -->
    @endif
@endif


@if(setting('analytics_system','none')=='google')
    @if(setting("analytics_id",'') != "UA-XXXXX-X")
        {{-- Google Analytics: change UA-XXXXX-X to be your site's ID. --}}
        <!-- GA {{ setting("analytics_enabled",0) }}-->
        @script
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','{{ setting("analytics_id",'') }}','auto');ga('send','pageview');
        @endscript
        <!-- /GA -->
    @else
        <!-- NO GA -->
    @endif
@endif
