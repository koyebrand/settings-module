@extends('backend.layouts.app')

@section('title', app_name() . ' | '.__('settings::general.name')  )

@section('breadcrumb-links')

@endsection



@section('content')
    <div class="row mt-4" id="backend-settings">
        <div class="col">
            <div class="card">

                <div class="card-header">

                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title mb-4">
                                <i class="fas fa-cogs text-primary"></i> {{ __( 'settings::general.name' ) }} <small class="faded"><small>{{ config('settings.name') }} | {{ setting('app_name') }}</small></small>
                            </h4>
                        </div><!--col-->
                        <div class="col-sm-6">
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">

                            </div><!--btn-toolbar-->
                        </div><!--col-->
                    </div><!--row-->

                    <ul class="nav nav-tabs card-header-tabs" role="tablist">
                        @foreach(config('settingsfields') as $section => $fields)
                            @if(!data_get($fields,'hidden',false))
                            <li class="nav-item">
                                <a class="nav-link @if($loop->index == 0) active @endif"
                                   data-toggle="tab"
                                   role="tab"
                                   href="#tab-{{$loop->index}}">
                                    <i class="{{ Arr::get($fields, 'icon', 'icon icon-settings') }}"></i>
                                    @lang(data_get($fields,'title').'')
                                </a>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </div>

                <form method="post"
                          {{--action="{{ route('admin.settings.store') }}"--}}
                          class="form-horizontal"
                          role="form">
                        {!! csrf_field() !!}

                    <div class="card-body">

                        <div class="tab-content" style="border: 0px solid #d4d3d3;">
                            @if(count(config('settingsfields', [])) )

                                @foreach(config('settingsfields') as $section => $fields)
                                    <div class="tab-pane fade card card-info @if($loop->index == 0) show active @endif"
                                         role="tabpanel"
                                         style="padding: 1rem;"
                                         id="tab-{{$loop->index}}">

                                        <div class="row">
                                            <div class="col-md-12">
                                                @foreach($fields['elements'] as $field)

                                                    @includeIf('settings::backend.fields.' . $field['type'])
                                                    @unless($loop->last)
                                                        <hr/>
                                                    @endunless
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                <!-- end panel for {{ $fields['title'] }} -->
                                @endforeach

                            @endif
                        </div>


                    </div>


                    <div class="card-footer text-right">

                        <button class="btn-primary btn">
                            {{ __('labels.general.buttons.save') }}
                        </button>

                    </div>

                </form>

            </div>
        </div><!--col-->
    </div><!--row-->
@endsection

@push('after-scripts')
<script>

$(function(){
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
      $(this).tab('show');
      var scrollmem = $('body').scrollTop() || $('html').scrollTop();
      window.location.hash = this.hash;
      $('html,body').scrollTop(scrollmem);
    });
  });

</script>
@endpush
