<div class="form-group row {{ $errors->has($field['name']) ? ' has-error' : '' }}">

    <label class="control-label col-md-3 text-right" for="{{ $field['name'] }}">@lang($field['label'])</label>

    <div class="col-md-9">
        <div class="form-check">
            <input type="radio"
                   name="{{ $field['name'] }}"
                   value="true"
                   @if(true === old($field['name'], setting($field['name']))) checked="checked" @endif
                   class="form-check-input {{ Arr::get( $field, 'class') }}"
                   id="{{ $field['name'] }}"
                   placeholder="{{ __($field['label']) }}">
            <label class="form-check-label">
                {{ __('strings.true') }}
            </label>
        </div>

        <div class="form-check">
            <input type="radio"
                   name="{{ $field['name'] }}"
                   value="false"
                   @if(false === old($field['name'], setting($field['name']))) checked="checked" @endif
                   class="form-check-input {{ Arr::get( $field, 'class') }}"
                   id="{{ $field['name'] }}"
                   placeholder="{{ __($field['label']) }}">
            <label class="form-check-label">
                {{ __('strings.false') }}
            </label>
        </div>

        @if(isset($field['description']))
            <small class="form-text text-muted">{{ __($field['description']) }}</small>
        @endif

        @if ($errors->has($field['name']))
            <small class="help-block">{{ $errors->first($field['name']) }}</small>
        @endif
    </div>
</div>
