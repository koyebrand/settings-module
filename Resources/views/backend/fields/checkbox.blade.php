<div class="form-group row {{ $errors->has($field['name']) ? ' has-error' : '' }}">
    <label class="control-label col-md-3 text-right" for="{{ $field['name'] }}">{{ __($field['label']) }}</label>

    <div class="col-md-9 form-check">
        <div class="checkbox">
            <input name="{{ $field['name'] }}"
                   value="true"
                   type="checkbox"
                   @if(old($field['name'], setting($field['name'])))
                   checked="checked" @endif >
            <label class="form-check-label">
                {{ isset($field['optiontext']) ? $field['optiontext']:__($field['label']) }}
            </label>

            @if ($errors->has($field['name']))
                <small class="help-block">{{ $errors->first($field['name']) }}</small>
            @endif
        </div>
    </div>
</div>
