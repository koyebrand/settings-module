<div class="form-group row {{ $errors->has($field['name']) ? ' has-error' : '' }}">
    <label class="control-label col-md-3 text-right" for="{{ $field['name'] }}">{{ __($field['label']) }}</label>

    <div class="col-md-9">
        <select name="{{ $field['name'] }}" class="form-control {{ Arr::get( $field, 'class') }}"
                id="{{ $field['name'] }}">
            @foreach(Arr::get($field, 'options', []) as $val => $label)
                <option @if( old($field['name'], setting($field['name'])) == $val ) selected
                        @endif value="{{ $val }}">{{ $label }}</option>
            @endforeach
        </select>

        @if(isset($field['description']))
            <small class="form-text text-muted">{{ __($field['description']) }}</small>
        @endif

        @if ($errors->has($field['name']))
            <small class="help-block">{{ $errors->first($field['name']) }}</small>
        @endif
    </div>
</div>
