<div class="form-group row {{ $errors->has($field['name']) ? ' has-error' : '' }}">
    <label class="control-label col-md-3 text-right" for="{{ $field['name'] }}">{{ __($field['label']) }}</label>

    <div class="col-md-9">
        <input type="{{ $field['type'] }}"
               name="{{ $field['name'] }}"
               value="{{ old($field['name'], setting($field['name'])) }}"
               class="form-control {{ Arr::get( $field, 'class') }}"
               id="{{ $field['name'] }}"
               placeholder="{{ __($field['label']) }}">

        @if(isset($field['description']))
            <small class="form-text text-muted">{{ __($field['description']) }}</small>
        @endif

        @if ($errors->has($field['name']))
            <small class="help-block">{{ $errors->first($field['name']) }}</small>
        @endif
    </div>
</div>
