<div class="form-group row {{ $errors->has($field['name']) ? ' has-error' : '' }}">
    <label class="control-label col-md-3 text-right" for="{{ $field['name'] }}">{{ __($field['label']) }}</label>

    <div class="col-md-9 form-check">

        <div class="checkbox d-flex align-items-center">
            {{ html()->label(
                    html()->checkbox($field['name'], old($field['name'], setting($field['name'])) , 'true')
                            ->class('switch-input')
                    . '<span class="switch-slider"></span><span class="switch-handle"></span>')
                ->class('switch switch-sm switch-pill switch-primary')
                ->style('margin-right:5px;')
                ->for( $field['name'] ) }}
            
            {{ html()->label(isset($field['optiontext']) ? $field['optiontext']:__($field['label']))->for($field['name']) }}
            @if ($errors->has($field['name']))
                <small class="help-block">{{ $errors->first($field['name']) }}</small>
            @endif
        </div>
        @isset($field['description'])
        <small class="text-muted"><i class="fa fa-info-circle" aria-hidden="true"></i> 
           {{ $field['description'] }}
        </small>
        @endisset
    </div>
</div>
