<?php

/**
 * Global helper to
 */

use Illuminate\Database\QueryException;

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        if (env('DB_PASSWORD') !== null) {
            if (is_null($key)) {
                return new Modules\Settings\Entities\Setting();
            }

            if (is_array($key)) {
                return Modules\Settings\Entities\Setting::set($key[0], $key[1]);
            }
        }


        try {
            $value = Modules\Settings\Entities\Setting::get($key);
        } catch (QueryException $e) {
            // The database migrations might not have been executed yet - so we return the default value to prevent
            // migrations from erroring out
            return $default;
        }

        return is_null($value) ? value($default) : $value;
    }
}
