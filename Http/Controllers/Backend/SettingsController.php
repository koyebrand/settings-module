<?php

namespace Modules\Settings\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Settings\Entities\Setting;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('settings::backend.index');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = Setting::getValidationRules();
        //ddd($rules);
        $data = $request->validate($rules);
        //dd($request);
        $validSettings = array_keys($rules);

        $fields = Setting::getDefinedSettingFields();
        //ddd($fields);
        
        foreach ($fields as $key => $item) {
            if (in_array($item['name'], $validSettings)) {
                Setting::add($item['name'], (isset($data[$item['name']]) ? $data[$item['name']] : ''), Setting::getDataType($item['name']));
            } elseif (!isset($item['rules']) && ($item['type'] == 'check' || $item['type'] == 'checkbox' || $item['type'] == 'switch')) {
                // no rule existing for this and "bool"?
                //echo isset($data[$item['name']])? 'true':'false'; exit;
                //dd($item);
                //echo $request->input($item['name'],'false'); exit;
                //$data[$item['name']] = isset($data[$item['name']])? 'true':'false';
                Setting::add($item['name'], $request->input($item['name'], 'false'), Setting::getDataType($item['name']));
            }
        }
        /*
        foreach ($data as $key => $val) {
            if (in_array($key, $validSettings)) {
                Setting::add($key, $val, Setting::getDataType($key));
            }
        }
        */

        return redirect()->back()->withFlashSuccess(__('alerts.backend.data.saved'));
    }
}
