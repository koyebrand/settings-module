<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for Backend:

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    //include_route_files(__DIR__.'/backend/');
    Route::group([
        'prefix'     => 'settings',
        'as'         => 'settings.',
        //'namespace'  => 'Crud',
    ], function () {
        Route::group([
            'middleware' => 'permission:edit settings',
        ], function () {
            Route::get('', 'SettingsController@index')->name('index');
            Route::post('', 'SettingsController@store')->name('store');

            //Route::get('datatabledata', 'SettingsController@datatabledata')->name('datatabledata');
            //Route::any('export/{filter?}', 'SettingsController@export')->name('export');

            //Route::get('edit/{id}', 'SettingsController@edit')->name('edit');
            //Route::get('create', 'SettingsController@create')->name('create');
            //Route::any('update/{id?}', 'SettingsController@update')->name('update');
            //Route::any('delete/{organisation}', 'SettingsController@destroy')->name('delete');
        });
    });
});


// for frontend:
Route::prefix('settings')->group(function () {
    Route::get('/', 'SettingsController@index');
});
