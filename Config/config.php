<?php

return [
    'name' => 'Settings',
    'iconclass' => 'fas fa-cogs',
    'section' => 'system',
    'cannotbedisabled' => true,
];
