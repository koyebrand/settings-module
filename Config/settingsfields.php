<?php

/**
 * IMPORTANT NOTE: to make everything more clear, names should be prefixed with the root key. E.g. "name" within "app"
 * should become "app_name"
 *
 */

return [
    'app'      => [
        'title' => 'settings::general.fields.app.title',
        'desc'  => 'App-Einstellungen allgemein.',
        'icon'  => 'icon icon-settings',

        'elements' => [
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'app_name',
                'label'       => 'settings::general.fields.app.app_name_label',
                'rules'       => 'required|min:2|max:50',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => env('SETTING_APP_NAME', 'My App'),
                'description' => 'settings::general.fields.app.app_name_description',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'app_brand_logo',
                'label'       => 'settings::general.fields.app.app_brand_logo_label',
                'rules'       => 'min:2|max:50',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => env('SETTING_APP_BRAND_LOGO', '/img/backend/brand/logo_kbc.svg'),
                'description' => 'settings::general.fields.app.app_brand_logo_description',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'app_brand_signet',
                'label'       => 'settings::general.fields.app.app_brand_signet_label',
                'rules'       => 'min:2|max:50',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => env('SETTING_APP_BRAND_LOGO', '/img/backend/brand/signet_kbc.svg'),
                'description' => 'settings::general.fields.app.app_brand_signet_description',
            ],
        ],
    ],

    'analytics'      => [
        'title' => 'settings::general.fields.analytics.title',
        'desc'  => 'Website-Tracking',
        'icon'  => 'icon icon-chart',
        'hidden' => true,
        'elements' => [
            [
                'type'        => 'select',
                'name'        => 'analytics_system',
                'label'       => 'settings::general.fields.analytics.system_label',
                'rules'       => '',
                'options'     => [
                    'none'    => 'Kein Tracking',
                    'google'  => 'Google-Analytics',
                    'own_matomo'  => 'Matomo selbstgehostet',
                ],
                'value'       => env('SETTINGS_ANALYTICS_SYSTEM', 'none'),
                'description' => 'settings::general.fields.analytics.system_description',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'analytics_id',
                'label'       => 'Tracking-ID/Site-ID',
                'rules'       => 'max:50',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'Z.B. UA-XXXXX-X bei Google oder 1 (Zahl) bei Matomo',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'analytics_tracker_url',
                'label'       => 'Tracker-URL',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'Z.B. //matomo.meinedomain.de - nur bei Matomo nötig',
            ]
        ],
    ],

    'uptimemonitor'      => [
        'title' => 'settings::general.fields.uptimemonitor.title',
        'desc'  => 'Monitoring der Server-Erreichbarkeit',
        'icon'  => 'icon icon-clock',
        'hidden' => true,
        'elements' => [
            [
                'type'        => 'bool',
                'name'        => 'uptimemonitor_enabled',
                'label'       => 'settings::general.fields.shared.active',
                'rules'       => 'required|in:true,false',
                'value'       => true,
                'description' => 'In Dashboard?',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'uptimemonitor_api_key',
                'label'       => 'API-Key (single, read only)',
                'rules'       => 'max:50',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'Z.B. m123456789-b1a234a1234a12a123aa12a1',
            ]

        ],
    ],
    'sociallinks' => [
        'title' => 'Links Social Networks',
        'desc'  => 'Links dieses Projektes z.B. für Footer',
        'icon'  => 'icon icon-social',
        'hidden' => true,
        'elements' => [
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'sociallinks_facebook',
                'label'       => 'Facebook-URL',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'https://facebook.com/projekt',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'sociallinks_instagram',
                'label'       => 'Instagram-URL',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'https://instagram.com/projekt',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'sociallinks_twitter',
                'label'       => 'Twitter',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'Twitter-Id - ohne "@"',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'sociallinks_linkedin',
                'label'       => 'LinkedIn-URL',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'https://linkedin.com/projekt',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'sociallinks_xing',
                'label'       => 'Xing-URL',
                'rules'       => 'max:100',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '',
                'description' => 'https://xing.com/projekt',
            ],
        ]
    ],
    'admin'      => [
        'title' => 'Admin',
        'desc'  => 'Active Sections',
        'icon'  => 'icon icon-list',

        'elements' => [
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_dashboard_active',
                'label'       => 'Dashboard',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'admin_dashboard_redirect',
                'label'       => 'Dashboard-Redirect',
                'rules'       => 'max:100',
                'value'       => '',
                'description' => 'URL',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_data_active',
                'label'       => 'Data',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_files_active',
                'label'       => 'Files',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_frontend_active',
                'label'       => 'Frontends',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_cms_active',
                'label'       => 'CMS',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_shop_active',
                'label'       => 'Shop',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ],
            [
                'type'        => 'bool',
                'name'        => 'admin_sidebar_system_active',
                'label'       => 'System',
                'rules'       => 'in:true,false',
                'value'       => true,
                'description' => 'In Sidebar?',
            ]

        ],
    ]

    /*
    'ordering' => [
        'title' => 'Sortierung',
        'desc'  => 'Artikel in Listen.',
        'icon'  => 'icon icon-list',
        'admin' => 'invisible',

        'elements' => [
            [
                'type'        => 'text',
                'data'        => 'integer',
                'name'        => 'ordered_articles_count',
                'label'       => 'Anzahl Artikel je Liste',
                'rules'       => 'required|min:1|max:10',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => 5,
                'description' => '',
            ],
            [
                'type'        => 'text',
                'data'        => 'string',
                'name'        => 'ordering_articles_home',
                'label'       => 'JSON',
                'rules'       => 'max:5000',
                // 'class' => 'w-auto px-2', // any class for input
                'value'       => '[]',
                'description' => '',
            ]

        ],
    ],

    */

    /*
    'email'    => [
        'title'    => 'Email',
        'desc'     => 'Email settings for app',
        'icon'     => 'icon icon-envelope',
        'elements' => [
            [
                'type'  => 'email',
                'name'  => 'email_from_email',
                'label' => 'settings.email.from_label',
                'rules' => 'sometimes|nullable|email',
                'value' => env('SETTING_EMAIL_DEFAULT_FROM', ''),
            ],
            [
                'type'  => 'text',
                'name'  => 'email_from_name',
                'label' => 'settings.email.from_name_label',
                'rules' => 'sometimes|nullable|min:2|max:50',
                'value' => env('SETTING_EMAIL_DEFAULT_FROM_NAME', ''),
            ],
            [
                'type'  => 'text',
                'name'  => 'email_email_subject',
                'label' => 'settings.email.subject_label',
                'rules' => 'sometimes|nullable|min:2|max:50',
                'value' => env('SETTING_EMAIL_DEFAULT_SUBJECT', ''),
            ],
        ],
    ],
    */
    /*
    'register' => [
        'title'    => 'Register',
        'desc'     => 'Sign up specific settings',
        'icon'     => 'icon icon-key',
        'elements' => [
            [
                'type'        => 'bool',
                'name'        => 'register_enabled',
                'label'       => 'settings.register.enabled_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_REGISTER_ENABLED', true),
                'description' => 'settings.register.enabled_description',
            ],
            [
                'type'        => 'bool',
                'name'        => 'register_captcha_enabled',
                'label'       => 'settings.register.captcha_enabled_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_REGISTER_CAPTCHA_ENABLED', true),
                'description' => 'settings.register.captcha_enabled_description',
            ],
        ],
    ],
    'login'    => [
        'title'    => 'Login',
        'desc'     => 'Login specific settings',
        'icon'     => 'icon icon-user',
        'elements' => [
            [
                'type'        => 'bool',
                'name'        => 'login_enabled',
                'label'       => 'settings.login.enabled_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_LOGIN_ENABLED', true),
                'description' => 'settings.login.enabled_description',
            ],
            [
                'type'        => 'bool',
                'name'        => 'login_captcha_enabled',
                'label'       => 'settings.login.captcha_enabled_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_LOGIN_CAPTCHA_ENABLED', false),
                'description' => 'settings.login.captcha_enabled_description',
            ],
            [
                'type'        => 'select',
                'name'        => 'login_captcha',
                'label'       => 'settings.login.captcha_label',
                'rules'       => 'required|in:captcha,recaptcha',
                'options'     => [
                    'captcha'   => 'Default Captcha',
                    'recaptcha' => 'Google reCaptcha',
                ],
                'value'       => env('SETTING_LOGIN_CAPTCHA', 'captcha'),
                'description' => 'settings.login.captcha_description',
            ],
            [
                'type'        => 'bool',
                'name'        => 'login_social_enabled',
                'label'       => 'settings.login.social_enabled_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_LOGIN_SOCIAL_ENABLED', false),
                'description' => 'settings.login.social_enabled_description',
            ],
            [
                'type'        => 'bool',
                'name'        => 'login_remember_me',
                'label'       => 'settings.login.remember_me_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_LOGIN_REMEMBER_ME', true),
                'description' => 'settings.login.remember_me_description',
            ],
            [
                'type'        => 'bool',
                'name'        => 'login_forgot_password_link',
                'label'       => 'settings.login.forgot_password_label',
                'rules'       => 'required|in:true,false',
                'value'       => env('SETTING_LOGIN_FORGOT_PASSWORD_LINK', true),
                'description' => 'settings.login.forgot_password_description',
            ],
            [
                'type'        => 'text',
                'name'        => 'login_redirect_uri',
                'label'       => 'settings.login.redirect_uri_label',
                'value'       => env('APP_URL'),
                'rules'       => 'required|url',
                'description' => 'settings.login.redirect_uri_description',
            ],
        ],
    ],
    */
];
