<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class Setting
 *
 * @package App\Models\System
 */
class Setting extends Model
{
    public const CACHE_KEY = 'settings.all';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param        $key
     * @param        $val
     * @param string $type
     *
     * @return bool
     */
    public static function add($key, $val, $type = 'string')
    {
        if (self::has($key)) {
            return self::set($key, $val, $type);
        }

        return self::create(['name' => $key, 'val' => $val, 'type' => $type]) ? $val : false;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return bool|int|mixed
     */
    public static function get($key, $default = null)
    {
        if (self::has($key)) {
            $setting = self::getAllSettings()->where('name', $key)->first();
            return self::castValue($setting->val, $setting->type);
        }

        return self::getDefaultValue($key, $default);
    }

    /**
     * @param string $key
     * @param mixed  $val
     * @param string $type
     *
     * @return bool
     */
    public static function set(string $key, $val, string $type = 'string')
    {
        if ($setting = self::getAllSettings()->where('name', $key)->first()) {
            return $setting->update([
                'name' => $key,
                'val' => $val,
                'type' => $type,
            ]) ? $val : false;
        }

        return self::add($key, $val, $type);
    }

    /**
     * Remove a setting
     *
     * @param string $key
     *
     * @return bool
     */
    public static function remove(string $key)
    {
        if (self::has($key)) {
            return self::whereName($key)->delete();
        }

        return false;
    }

    /**
     * Check if setting exists
     *
     * @param string $key
     *
     * @return bool
     */
    public static function has(string $key): bool
    {
        return (bool)self::getAllSettings()->whereStrict('name', $key)->count();
    }

    /**
     * Get the validation rules for setting fields
     *
     * @return array
     */
    public static function getValidationRules(): array
    {
        return self::getDefinedSettingFields()->pluck('rules', 'name')
            ->reject(function ($val) {
                return is_null($val);
            })->toArray();
    }

    /**
     * Get the data type of a setting
     *
     * @param string $field
     *
     * @return string
     */
    public static function getDataType(string $field): string
    {
        $type = self::getDefinedSettingFields()
            ->pluck('data', 'name')
            ->get($field);

        return is_null($type) ? 'string' : (string)$type;
    }

    /**
     * Get default value for a setting
     *
     * @param string $field
     *
     * @return mixed
     */
    public static function getDefaultValueForField(string $field)
    {
        return self::getDefinedSettingFields()
            ->pluck('value', 'name')
            ->get($field);
    }

    /**
     * Get default value from config if no value passed
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    private static function getDefaultValue(string $key, $default)
    {
        return is_null($default) ? self::getDefaultValueForField($key) : $default;
    }

    /**
     * Get all the settings fields from config
     *
     * @return Collection
     */
    public static function getDefinedSettingFields()
    {
        return collect(config('settingsfields'))
            ->pluck('elements')
            ->flatten(1);
    }

    /**
     * caste value into respective type
     *
     * @param $val
     * @param $castTo
     *
     * @return bool|int
     */
    private static function castValue($val, $castTo)
    {
        switch ($castTo) {
            case 'int':
            case 'integer':
                return intval($val);
                break;

            case 'bool':
            case 'switch':
            case 'check':
            case 'checkbox':
            case 'boolean':
                if ($val == 'true') {
                    return true;
                } elseif ($val == 'false') {
                    return false;
                } else {
                    // boolval will make ANY string to true, so 'false' will be true, which is not intended!
                    return boolval($val);
                }

            case 'string':
                // We don't care about string if it's a boolean!
                if ($val == 'true') {
                    return true;
                } elseif ($val == 'false') {
                    return false;
                } else {
                    // Everything else is a string
                    return (string)$val;
                }

            default:
                return $val;
        }
    }

    /**
     * Get all the settings
     *
     * @return mixed
     */
    public static function getAllSettings()
    {
        return Cache::rememberForever(static::CACHE_KEY, function () {
            return self::all();
        });
    }

    /**
     * Flush the cache
     */
    public static function flushCache()
    {
        Cache::forget(static::CACHE_KEY);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::updated(function () {
            self::flushCache();
        });

        static::created(function () {
            self::flushCache();
        });
    }
}
