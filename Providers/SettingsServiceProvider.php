<?php

namespace Modules\Settings\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Module;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Settings', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Settings', 'Config/config.php') => config_path('settings.php'),
            module_path('Settings', 'Config/settingsfields.php') => config_path('settingsfields.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Settings', 'Config/config.php'),
            'settings'
        );

        foreach (Module::getOrdered() as $key => $item) {
            if (file_exists(module_path($key, 'Config/settingsfields.php'))) {
                $this->mergeConfigFrom(
                    module_path($key, 'Config/settingsfields.php'),
                    'settingsfields'
                );
            }
        }

        $this->mergeConfigFrom(
            module_path('Settings', 'Config/settingsfields.php'),
            'settingsfields'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/settings');

        $sourcePath = module_path('Settings', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/settings';
        }, \Config::get('view.paths')), [$sourcePath]), 'settings');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/settings');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'settings');
        } else {
            $this->loadTranslationsFrom(module_path('Settings', 'Resources/lang'), 'settings');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            // DEPRECATED: uses faker, which is not supported anymore: app(Factory::class)->load(module_path('Settings', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
